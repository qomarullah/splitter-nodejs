package main

import (
	"bufio"
	"fmt"
	copydir "github.com/otiai10/copy"
	"log"
	"os"
	"strings"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var depServicesAll []string
var depHelpersAll []string

func main() {

	basePath := os.Getenv("project")
	filePath := basePath + "/server/api/inbox.js"
	filePathAuth := basePath + "/server/authentication/tokenSchemeForgeRockHelper.js"

	if basePath == "" {
		log.Fatalf("please set env variable \"project\"")
	}
	if len(os.Args) > 1 {
		if len(os.Args) > 2 {
			filePath = basePath + os.Args[2]
		}
		//collect dependency
		servicePath := basePath + "/server/services"
		helperPath := basePath + "/server/helpers"

		checkDep("auth", servicePath, helperPath, filePathAuth)
		checkDep("auth", servicePath, helperPath, filePathAuth)

		checkDep("api", servicePath, helperPath, filePath)
		checkDep("api", servicePath, helperPath, filePath)

		depServicesAll = removeDuplicates(depServicesAll)
		depHelpersAll = removeDuplicates(depHelpersAll)

		if os.Args[1] == "print" {
			print(servicePath, helperPath)
		}
		if os.Args[1] == "generate" {
			generate(basePath, servicePath, helperPath)

		}

	}

}
func generate(basePath, servicePath, helperPath string) {
	t := time.Now()
	timestamp := t.Format("20060102150405")

	basePathNew := basePath + timestamp
	start := time.Now()
	fmt.Println("copy--------------------------------" + basePath + "," + basePathNew)
	err := copydir.Copy(basePath, basePathNew)
	if err != nil {
		log.Fatalf("failed copy dir: %s", err)
	}
	duration := time.Since(start)
	fmt.Println("duration", duration)
}
func print(servicePath, helperPath string) {
	fmt.Println("dependency-services--------------------------------")
	for _, eachFile := range depServicesAll {
		//fmt.Println(servicePath + eachFile)
		fmt.Println(eachFile)

	}
	fmt.Println("dependency-helpers--------------------------------")

	for _, eachFile := range depHelpersAll {
		//fmt.Println(helperPath + eachFile)
		fmt.Println(eachFile)

	}
}

func checkDep(serviceOrHelpers, servicePath, helperPath, filejs string) {

	var file *os.File
	var err error
	if strings.HasSuffix(filejs, ".js.js") {
		filejs = strings.Replace(filejs, ".js.js", ".js", -1)
		fmt.Println("========================================ada" + filejs)
	}
	if serviceOrHelpers == "services" {
		fmt.Println(serviceOrHelpers + "=========>" + servicePath + filejs)
		file, err = os.Open(servicePath + filejs)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
	} else if serviceOrHelpers == "helpers" {
		fmt.Println(serviceOrHelpers + "=========>" + helperPath + filejs)
		file, err = os.Open(helperPath + filejs)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)
		}
	} else {
		fmt.Println(serviceOrHelpers + "=========>" + filejs)
		file, err = os.Open(filejs)
		if err != nil {
			log.Fatalf("failed opening file: %s", err)

		}
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var depServices []string
	var depHelpers []string

	for scanner.Scan() {

		line := scanner.Text()
		if strings.Index(line, "require") != -1 {
			if strings.Index(line, "services") != -1 {
				test := strings.Split(line, "services")[1]
				test2 := strings.Split(test, "'")[0] + ".js"
				depServices = append(depServices, test2)
				depServicesAll = append(depServicesAll, test2)
			}
			if strings.Index(line, "helpers") != -1 {
				test := strings.Split(line, "helpers")[1]
				test2 := strings.Split(test, "'")[0] + ".js"
				depHelpers = append(depHelpers, test2)
				depHelpersAll = append(depHelpersAll, test2)
			}
		}

	}

	file.Close()

	for _, eachFile := range depServices {
		//fmt.Println(servicePath + eachFile)
		checkDep("services", servicePath, helperPath, eachFile)

	}
	for _, eachFile := range depHelpers {
		//fmt.Println(helperPath + eachFile)
		checkDep("helpers", servicePath, helperPath, eachFile)
	}
}

func removeDuplicates(a []string) []string {

	mymap := map[string]bool{}
	result := []string{}

	for v := range a {
		if mymap[a[v]] == true {
		} else {
			mymap[a[v]] = true
			result = append(result, a[v])
		}
	}
	return result
}
